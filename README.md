Import Livejournal dumps to WordPress
=====================================

Convert LiveJournal backups (as produced by ljdump) to WordPress's CSV import
format as used by the [CSV Importer
plugin](https://wordpress.org/plugins/csv-importer/).

WordPress's [current
plugin](https://wordpress.org/plugins/livejournal-importer/) to import
LiveJournal entries has some limitations:

1. it only supports LiveJournal, not API-compatible sites like Dreamwidth
2. it does not support content from communities

This script is one way to import LiveJournal-like sites, communities, etc.
 
This Python 3 script takes the output of Greg Hewgill's
[ljdump](http://hewgill.com/ljdump/) script and produces a CSV file, which can
be imported using the [CSV Importer
plugin](https://wordpress.org/plugins/csv-importer/).


Required software
-----------------

Python 3.2 or later

Usage
-----

    usage: convert.py [-h] [--split] input_directory output_file_or_directory
    
    Convert ljdump output to CSV input for WordPress.
    
    positional arguments:
      input_directory       Directory containing the output of ljdump (files named
                            eg C-111 or L-27)
      output_file_or_directory
                            Filename or directory for the output
    
    optional arguments:
      -h, --help            show this help message and exit
      --split               Split the output into multiple files for easier upload

The full process to import a journal would be:

 1. set up and run [ljdump](http://hewgill.com/ljdump/) on your journal
 2. run this script on the output of ljdump to transform it into a CSV file
 3. install the CSV Importer plugin
 4. go to Tools → CSV Importer and select the CSV file to import

Limitations
-----------

Only self-hosted WordPress installs (Wordpress.org software) support CSV
imports via the CSV Importer plugin. For WordPress.com blogs, you will need to
use [one of their standard import
mechanisms](http://en.support.wordpress.com/import/).

Embedding code (such as videos) and polls are not properly exported by ljdump
and thus won't be transferred to the CSV. Errors will be printed to standard error.

Cut tags are not supported, all entries will be imported and displayed in full.
(Pull requests welcome to add WordPress more tags.)

#!/usr/bin/env python3

import argparse, csv, os, sys
import xml.etree.ElementTree as etree
from collections import namedtuple as nt

# CUSTOMISABLE SETTINGS
ANON_NAME = "Anonymous"
ANON_URL = ""

ENTRIES_PER_FILE=150

Entry = nt('Entry', ['csv_post_title', 'csv_post_post', 'csv_post_type',
        'csv_post_tags', 'csv_post_date'])

NUM = "NUM"

Comment = nt('Comment', ['csv_comment_%s_author' % NUM,
        'csv_comment_%s_url' % NUM,
        'csv_comment_%s_content' % NUM,
        'csv_comment_%s_date' % NUM,
        # not provided because ljdump output does not contain commenter emails
        # 'csv_comment_2_author_email' % NUM
    ])

def get_text(tree, xpath):
    """
    Get the text from a node with xpath, if it exists, or return ""
    """
    elem = tree.find(xpath)
    if elem is not None:
        return elem.text
    return ""

def repair_content(content, url, path):
    content = content.replace("<raw-code>", "")
    content = content.replace("</raw-code>", "")

    if "<poll" in content or "<lj-poll" in content:
        print("Unable to import poll tag in %s (%s), leaving as-is" %
                (path, url))
    if "<site-embed" in content or "<lj-embed" in content:
        print("Unable to import embed tag in %s (%s), leaving as-is" %
                (path, url))
    return content

def get_user(comment_elem):
    user_elem = comment_elem.find('user')
    if user_elem is not None:
        user = user_elem.text
        url = 'http://%s.dreamwidth.org/' % user
    else:
        user = ANON_NAME
        url = ANON_URL
    return user, url

class Entries:

    def __init__(self, input_directory):
        self.input_directory = input_directory
        self.entries = {}
        self.comments = {}
        self.max_comments = 0

    def add_entry(self, filename):
        entrynum = filename[2:]
        path = os.path.join(self.input_directory, filename)
        tree = etree.parse(path)
        url = get_text(tree, 'url')
        self.entries[entrynum] = Entry(
            get_text(tree, 'subject'),
            repair_content(get_text(tree, 'event'), url, path),
            'post', # type is always post
            get_text(tree, 'props/taglist'),
            get_text(tree, 'eventtime'),
        )

    def add_comments(self, filename):
        entrynum = filename[2:]
        comment_list = self.comments.setdefault(entrynum, [])
        # author, url, content, date
        tree = etree.parse(os.path.join(self.input_directory, filename))
        for comment_elem in tree.findall('comment'):
            user, url = get_user(comment_elem)
            
            comment_list.append(Comment(
                user, url,
                comment_elem.find('body').text,
                comment_elem.find('date').text,
            ))
        if len(comment_list) > self.max_comments:
            self.max_comments = len(comment_list)

    def get_header_row(self):
        header_row = list(Entry._fields)
        for i in range(1, self.max_comments+1):
            header_row.extend([field.replace(NUM, str(i))
                    for field in Comment._fields])
        return header_row

    def build_entry_row(self, entrynum):
        row = list(self.entries[entrynum])
        for comment in self.comments.get(entrynum, []):
            row.extend(list(comment))
        return row

    def output(self, output_file):
        f = open(output_file, 'w')
        writer = csv.writer(f)
        writer.writerow(self.get_header_row())
        for entrynum in self.entries:
            writer.writerow(self.build_entry_row(entrynum))
        f.close()

    def get_new_fh(self, output_dir):
        file_template = "output-%d.csv"
        f = open(os.path.join(output_dir, file_template % self.filenum), 'w')
        writer = csv.writer(f)
        writer.writerow(self.get_header_row())
        self.filenum += 1
        return f, writer

    def output_split(self, output_dir):
        os.makedirs(output_dir, exist_ok=True)
        self.filenum = 1

        f, writer = self.get_new_fh(output_dir)

        entry_counter = 0

        for entrynum in self.entries:
            if entry_counter > ENTRIES_PER_FILE:
                f.close()
                f, writer = self.get_new_fh(output_dir)
                entry_counter = 0
 
            writer.writerow(self.build_entry_row(entrynum))
            entry_counter += 1      

        f.close()

    def run(self):
        for filename in os.listdir(self.input_directory):
            if filename[:2] == "L-":
                self.add_entry(filename)
            elif filename[:2] == "C-":
                self.add_comments(filename)

def convert(input_directory, output_file, split):
    entries = Entries(input_directory)
    entries.run()

    if split:
        entries.output_split(output_file)
    else:
        entries.output(output_file)

def main():
    parser = argparse.ArgumentParser(description='Convert ljdump output to CSV input for WordPress.')
    parser.add_argument('--split', dest='split', action='store_true',
                       help='Split the output into multiple files for easier upload')
    parser.add_argument(dest='input_directory', metavar='input_directory', action='store',
                       help='Directory containing the output of ljdump (files named eg C-111 or L-27)')
    parser.add_argument(metavar='output_file_or_directory', dest='output', action='store',
                       help='Filename or directory for the output')

    args = parser.parse_args()

    convert(args.input_directory, args.output, args.split)

if __name__ == '__main__':
    main()
